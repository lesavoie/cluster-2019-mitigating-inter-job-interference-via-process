/* 
   Argonne Leadership Computing Facility benchmark
   BlueGene/P version
   Bi-section bandwidth
   Written by Vitali Morozov <morozov@anl.gov>
   Modified by Daniel Faraj <faraja@us.ibm.com>
   
   Measures a partition minimal bi-section bandwidth
*/    
#include <string.h>

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <scorep/SCOREP_User.h>

#define NN_DEBUG

int rand_range(int M, int N) {
  return M + rand() / (RAND_MAX / (N - M + 1) + 1);
}

main( int argc, char *argv[] )
{
  int ppn, taskid, ntasks, is1, ir1, i, k;
  char *sb, *rb;
  double d, d1;
  MPI_Status stat[2];
  MPI_Request req[2];
  double t1, elapsed;
  struct timespec compTime;
  char imbalanceDesc[10];
 
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &taskid );
  MPI_Comm_size( MPI_COMM_WORLD, &ntasks );
   
  if (argc != 6) {
    if (taskid == 0) printf("Usage: %s <message size> <iterations> <comp time per iter> <comp imbalance> <random seed>\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  int msgSize, iterations, maxSleepTime, sleepTime, compImbalance, seed;
  msgSize = atoi(argv[1]);
  iterations = atoi(argv[2]);
  maxSleepTime = atoi(argv[3]);
  compImbalance = atoi(argv[4]);
  seed = atoi(argv[5]);

  int toTime = iterations / 3;

  int minComp;
  srand(taskid + seed);
  switch(compImbalance) {
    case 1: // Small
      sleepTime = rand_range(maxSleepTime * 0.9, maxSleepTime);
      strcpy(imbalanceDesc, "Small");
      break;
    case 2: // Medium
      sleepTime = rand_range(0, maxSleepTime);
      strcpy(imbalanceDesc, "Medium");
      break;
    case 3: // Large
      if (taskid == 0) sleepTime = maxSleepTime;
      else sleepTime = 0;
      strcpy(imbalanceDesc, "Large");
      break;
    default:
      if (taskid == 0) printf("Invalid computation imbalance. Valid values are 1 (small), 2 (medium), and 3 large)\n");
      MPI_Abort(MPI_COMM_WORLD, 2);
      break;
  }

  compTime.tv_sec = 0;
  compTime.tv_nsec = sleepTime;

#ifdef NN_DEBUG
  if (taskid == 0) {
    printf("Message size: %d bytes\n", msgSize);
    printf("Iterations: %d\n", iterations);
    printf("Max computation time: %d ns\n", maxSleepTime);
    printf("Imbalance: %s\n", imbalanceDesc);
    printf("Random seed: %d\n", seed);
  }
  printf("Rank %d computation time: %d ns\n", taskid, sleepTime);
  fflush(stdout);
#endif

  posix_memalign( (void **)&sb, 16, sizeof( char ) * msgSize );
  posix_memalign( (void **)&rb, 16, sizeof( char ) * msgSize );

  if ( getranks( taskid, &is1, &ir1, &ppn ) == 0 )
  {
    d = 0.0;
    elapsed = 0.0;
        
    char region_name[30];
    MPI_Barrier (MPI_COMM_WORLD);
    SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_MainLoop", SCOREP_USER_REGION_TYPE_COMMON);
    SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_ForTiming", SCOREP_USER_REGION_TYPE_COMMON);

    if ( taskid == is1 )
    {

      t1 = MPI_Wtime();
      for ( k = 0; k < iterations; k++ )
      {
        sprintf(region_name, "TRACER_WallTime_Iteration%d", k);
        SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);
        if (sleepTime > 0) nanosleep(&compTime, NULL);
        MPI_Isend( sb, msgSize, MPI_BYTE, ir1, is1, MPI_COMM_WORLD, &req[0] );
        MPI_Irecv( rb, msgSize, MPI_BYTE, ir1, ir1, MPI_COMM_WORLD, &req[1] );
        MPI_Waitall( 2, req, stat );
        SCOREP_USER_REGION_BY_NAME_END(region_name);
        if (k == toTime) {
          printf("Rank %d: end timing 1\n", taskid);
          SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_ForTiming");
        }
      }
      t1 = MPI_Wtime() - t1;
      elapsed = t1;
      
      t1 /= (double)iterations;
      d = 2 * (double)msgSize / t1;
    }

    if ( taskid == ir1 )
    {
      t1 = MPI_Wtime();
      for ( k = 0; k < iterations; k++ )
      {
        sprintf(region_name, "TRACER_WallTime_Iteration%d", k);
        SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);
        if (sleepTime > 0) nanosleep(&compTime, NULL);
        MPI_Isend( sb, msgSize, MPI_BYTE, is1, ir1, MPI_COMM_WORLD, &req[0] );
        MPI_Irecv( rb, msgSize, MPI_BYTE, is1, is1, MPI_COMM_WORLD, &req[1] );
        MPI_Waitall( 2, req, stat );
        SCOREP_USER_REGION_BY_NAME_END(region_name);
        if (k == toTime) {
          printf("Rank %d: end timing 2\n", taskid);
          SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_ForTiming");
        }
      }
      elapsed = MPI_Wtime() - t1;
    }
    SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_MainLoop");

    printf("Rank %d elapsed time: %f\n", taskid, elapsed);
    if ( taskid == 0 ) {
      printf("PPN: %d Bisection BW(GB/s): %.2lf\n", ppn, d1 / 1e9 );
    }
  }

  MPI_Finalize();
}
