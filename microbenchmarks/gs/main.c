/*
 * Simple global synchronization benchmark.
 */

#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <scorep/SCOREP_User.h>
#include <time.h>

#define NN_DEBUG

int rand_range(int M, int N) {
  return M + rand() / (RAND_MAX / (N - M + 1) + 1);
}

int main(int argc, char ** argv) {
  MPI_Init(&argc, &argv);

  int rank, ranks;
  MPI_Comm_size(MPI_COMM_WORLD, &ranks);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (argc != 6) {
    if (rank == 0) printf("Usage: %s <message size> <iterations> <comp time per iter> <comp imbalance> <random seed>\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  int msgSize, iterations, maxSleepTime, sleepTime, compImbalance, seed;
  msgSize = atoi(argv[1]);
  iterations = atoi(argv[2]);
  maxSleepTime = atoi(argv[3]);
  compImbalance = atoi(argv[4]);
  seed = atoi(argv[5]);

  int minComp;
  srand(rank + seed);
  char imbalanceDesc[10];
  switch(compImbalance) {
    case 1: // Small
      sleepTime = rand_range(maxSleepTime * 0.9, maxSleepTime);
      strcpy(imbalanceDesc, "Small");
      break;
    case 2: // Medium
      sleepTime = rand_range(0, maxSleepTime);
      strcpy(imbalanceDesc, "Medium");
      break;
    case 3: // Large
      if (rank == 0) sleepTime = maxSleepTime;
      else sleepTime = 0;
      strcpy(imbalanceDesc, "Large");
      break;
    default:
      if (rank == 0) printf("Invalid computation imbalance. Valid values are 1 (small), 2 (medium), and 3 large)\n");
      MPI_Abort(MPI_COMM_WORLD, 2);
      break;
  }

#ifdef NN_DEBUG
  if (rank == 0) {
    printf("Message size: %d bytes\n", msgSize);
    printf("Iterations: %d\n", iterations);
    printf("Max computation time: %d ns\n", maxSleepTime);
    printf("Imbalance: %s\n", imbalanceDesc);
    printf("Random seed: %d\n", seed);
  }
  printf("Rank %d computation time: %d ns\n", rank, sleepTime);
  fflush(stdout);
#endif

  int sleepSec = sleepTime / 1000000000;
  int sleepNSec = sleepTime % 1000000000;
  struct timespec compTime = {sleepSec, sleepNSec};

  int sendBuf[msgSize * ranks], recvBuf[msgSize * ranks];
  double *times = malloc((iterations + 1) * sizeof(double));
  times[0] = MPI_Wtime();

  int toTime = iterations / 3;

  SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_MainLoop", SCOREP_USER_REGION_TYPE_COMMON);
  SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_ForTiming", SCOREP_USER_REGION_TYPE_COMMON);
  for (int i = 0; i < iterations; ++i) {
    char region_name[30];
    sprintf(region_name, "TRACER_WallTime_Iteration%d", i);
    SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);

    if (sleepTime > 0) nanosleep(&compTime, NULL);
    MPI_Alltoall(sendBuf, msgSize, MPI_INT, recvBuf, msgSize, MPI_INT, MPI_COMM_WORLD);
    times[i+1] = MPI_Wtime();

    SCOREP_USER_REGION_BY_NAME_END(region_name);
    if (i == toTime) SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_ForTiming");
  }
  SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_MainLoop");

  // Gather the running times
  double myTime = times[iterations] - times[0];
  double maxTime;
  MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  if (rank == 0) printf("Execution time: %f\n", maxTime);
  for (int i = 0; i < iterations; ++i) {
    printf("Rank %d, iteration %d elapsed time: %f\n", rank, i, times[i+1] - times[i]);
  }

  MPI_Finalize();
  return 0;
}
