#!/bin/bash -x

#MSUB -q pbatch
#MSUB -l nodes=32
#MSUB -l partition=catalyst
#MSUB -l walltime=00:20:00
#MSUB -V
#MSUB -m ae

NODES=32
RANKS=$[$NODES * 1]

export SCOREP_ENABLE_TRACING=true

date
srun -l -N $NODES -n $RANKS ${HOME}/AMG2013/test/amg2013 -in ${HOME}/AMG2013/test/sstruct.in.MG.FD -pooldist 0 -P 2 4 4 -r 8 8 8 -printstats
date
