#!/usr/bin/env python

import argparse
import copy
import csv
import fractions
import os
import random
import shutil
import subprocess
import sys
import time

import pdb

# These variables can be changed to change the behavior of the script
NUM_ITERATIONS = 500
TRACE_SEED = 0
DEBUG = False  # Don't actually run the commands
DO_CHDIR = True  # Helpful to set this to false when using pdb

# These variables can be changed, but their correct values depend on things outside this script
HOME = os.environ['HOME']
SCRIPT_DIR = os.path.join(HOME, 'per-rank-qos')
STANDARD_NUM_TRACE_VERSIONS = 5
SIMS_PER_NODE = 16
TRACES_DIR = os.path.join("/p", "lustre1", "savoie1", 'scorep-traces')

def chdir(directory):
  if DO_CHDIR:
    os.chdir(directory)

def mkdir(directory):
  if DO_CHDIR:
    os.mkdir(directory)

def remove(file):
  if os.path.isfile(file):
    try:
      os.remove(file)
    except:
      # Ignore exceptions - it's not a disaster if the remove fails
      pass

def printTime(msg):
  print '---->' + msg + ": " + time.strftime("%Y-%m-%d %H:%M:%S")
  sys.stdout.flush()

# Represents a scorep trace that can be run through tracer
class Trace:
  def __init__(self, directory, ranks, nnRowSize=0):
    self.name = directory 
    self.tracePrefix = os.path.join(TRACES_DIR, directory)
    self.ranks = ranks
    self.nnRowSize = nnRowSize
    if self.name.endswith('p'):
      self.numVersions = NUM_ITERATIONS
      self.randomVersion = False
    else:
      self.numVersions = STANDARD_NUM_TRACE_VERSIONS
      self.randomVersion = True

  def checkParams(self):
    for i in range(self.numVersions):
      fileName = os.path.join(self.tracePrefix + str(i), "traces.otf2")
      assert os.path.isfile(fileName), "Trace file %s is missing" % fileName

allTraces = []

allTraces.append(Trace('bs8c50r2m', 8))
allTraces.append(Trace('nn8c50r2m', 8, 4))
allTraces.append(Trace('rp8c50r2m', 8))
allTraces.append(Trace('gs8c50r2m', 8))

allTraces.append(Trace('bs8c25r2m', 8))
allTraces.append(Trace('nn8c25r2m', 8, 4))
allTraces.append(Trace('rp8c25r2m', 8))
allTraces.append(Trace('gs8c25r2m', 8))

allTraces.append(Trace('bs8c75r2m', 8))
allTraces.append(Trace('nn8c75r2m', 8, 4))
allTraces.append(Trace('rp8c75r2m', 8))
allTraces.append(Trace('gs8c75r2m', 8))

allTraces.append(Trace('bs8c0r2m', 8))
allTraces.append(Trace('nn8c0r2m', 8, 4))
allTraces.append(Trace('rp8c0r2m', 8))
allTraces.append(Trace('gs8c0r2m', 8))

allTraces.append(Trace('bs8c50r2s', 8))
allTraces.append(Trace('nn8c50r2s', 8, 4))
allTraces.append(Trace('rp8c50r2s', 8))
allTraces.append(Trace('gs8c50r2s', 8))

allTraces.append(Trace('bs8c50r2l', 8))
allTraces.append(Trace('nn8c50r2l', 8, 4))
allTraces.append(Trace('rp8c50r2l', 8))
allTraces.append(Trace('gs8c50r2l', 8))

allTraces.append(Trace('bs80c50r2m', 80))
allTraces.append(Trace('nn80c50r2m', 80, 10))
allTraces.append(Trace('rp80c50r2m', 80))
allTraces.append(Trace('gs80c50r2m', 80))

allTraces.append(Trace('bs20c50r2m', 20))
allTraces.append(Trace('nn20c50r2m', 20, 4))
allTraces.append(Trace('rp20c50r2m', 20))
allTraces.append(Trace('gs20c50r2m', 20))

allTraces.append(Trace('bs20c25r2m', 20))
allTraces.append(Trace('nn20c25r2m', 20, 4))
allTraces.append(Trace('rp20c25r2m', 20))
allTraces.append(Trace('gs20c25r2m', 20))

allTraces.append(Trace('nn8c50r2p', 8, 4))

allTraces.append(Trace('mg32c50r2m', 32))

allTraces.append(Trace('cr32v', 32))
allTraces.append(Trace('mg32v', 32))
allTraces.append(Trace('pd32v', 32))
allTraces.append(Trace('pf32v', 32))

class SLAssignment:
  def __init__(self, rank, SL):
    self.rank = rank
    self.SL = SL

  def __repr__(self):
    return str(self.rank) + "-" + str(self.SL)

def updateName(name, current, count):
  name.append(current)
  if count > 1:
    name.append(str(count))

def createName(traces):
  assert len(traces) > 0, "Cannot create name from 0 traces"
  name = []
  current = traces[0]
  count = 1
  for trace in traces[1:]:
    if trace == current:
      count += 1
    else:
      updateName(name, current, count)
      current = trace
      count = 1
  updateName(name, current, count)
  return "-".join(name)

# Represents a test to run in tracer
class Test:
  globalNum = 1

  def __init__(self, traces, serviceLevels, slAssignments, nameSuffix, rpn = 1, beginNode = -1, endNode = -1, totalNodes = -1):
    self.traces = traces
    self.serviceLevels = serviceLevels
    self.slAssignments = slAssignments
    self.rpn = rpn
    self.beginNode = beginNode
    self.endNode = endNode
    self.totalNodes = totalNodes
    self.calcParams(nameSuffix)
    self.checkParams()

  # Calculate params that are based on params provide by the user
  def calcParams(self, nameSuffix):
    self.nodes = 0
    self.ranks = 0
    for trace in self.traces:
      assert trace.ranks % self.rpn == 0, "Number of ranks (%d) in trace %s do not divide evenly by RPN (%d) in test %s" % (trace.ranks, trace.name, self.rpn, self.name)
      self.nodes = self.nodes + (trace.ranks / self.rpn)
      self.ranks = self.ranks + trace.ranks
    if self.beginNode < 0:
      self.beginNode = 0
    if self.endNode < 0:
      self.endNode = self.beginNode + self.nodes - 1
    if self.totalNodes < 0:
      self.totalNodes = self.nodes
    if not self.serviceLevels:
      self.serviceLevels = [1]
    self.numSLs = len(self.serviceLevels)
    self.numJobs = len(self.traces)
    self.traceVersions = [0 for i in range(self.numJobs)] # Will be filled in later when the test runs
    self.name = createName([trace.name for trace in self.traces]) + "_" + nameSuffix
    if self.slAssignments is None:
      self.slAssignments = [[] for i in range(self.numJobs)]
    if len(self.name) < 200:
      self.shortName = self.name
    else:
      self.shortName = "Test_{0}_{1}".format(Test.globalNum, nameSuffix)
      Test.globalNum += 1

  # Check that the parameters make sense
  def checkParams(self):
    assert self.numSLs >= 1, "Must have at least 1 service level in every test"
    assert self.ranks >= self.numSLs, "Can't have more service levels than ranks"
    assert len(self.slAssignments) == self.numJobs, "Must have a service level assignment for every job"
    assert self.endNode - self.beginNode + 1 == self.nodes, "Mismatch between allocated nodes and nodes required. Begin: %d, end: %d, nodes: %d." % (self.beginNode, self.endNode, self.nodes)
    assert self.totalNodes >= self.nodes, "Can't map a job when the total amount of nodes is less than the number of nodes needed"
    assert all(sl > 0 for sl in self.serviceLevels), "Service level packet numbers must be greater than 0"

def calcJobIters(rankIters):
  jobIters = [None] * len(rankIters)
  for i in range(len(jobIters)):
    jobIters[i] = min(rankIters[i].values())
  return jobIters

def speedup(orig, modified):
  return (float(modified) - float(orig)) / float(orig)

class TestTiming:
  def __init__(self, test, rankAlgIters, rankFullIters, defRankFullIters):
    self.test = test
    self.rankAlgIters = rankAlgIters
    self.rankFullIters = rankFullIters
    self.defRankFullIters = defRankFullIters
    self.jobAlgIters = calcJobIters(self.rankAlgIters)
    self.jobFullIters = calcJobIters(self.rankFullIters)
    self.defJobFullIters = calcJobIters(self.defRankFullIters)
    self.jobAlgSpeedups = None
    self.overallAlgSpeedup = None
    self.jobFullSpeedups = None
    self.overallFullSpeedup = None

  def calcSpeedupInternal(self, selfIters, otherIters):
    jobSpeedups = [speedup(otherIters[i], selfIters[i]) for i in range(len(selfIters))]
    # Overall is the average of the job speedups
    overallSpeedup = sum(jobSpeedups) / len(jobSpeedups)
    return (jobSpeedups, overallSpeedup)

  def calcSpeedupFrom(self, other):
    if other == None:
      other = self
    (self.jobAlgSpeedups, self.overallAlgSpeedup) = self.calcSpeedupInternal(self.jobAlgIters, other.jobAlgIters)
    (self.jobFullSpeedups, self.overallFullSpeedup) = self.calcSpeedupInternal(self.jobFullIters, self.defJobFullIters)

  def printSpeedup(self):
    print self.test.name
    print "({0})".format(self.test.shortName)
    print "Service levels: {0}".format(self.test.serviceLevels)
    print "Assignments: {0}".format(self.test.slAssignments)
    print "Speedup: {0}".format(self.overallAlgSpeedup * 100)

def averageTiming(timings):
  rankAlgIters = dict()
  rankFullIters = dict()
  defRankFullIters = dict()
  for timing in timings:
    for job in timing.rankAlgIters:
      if job not in rankAlgIters:
        rankAlgIters[job] = dict()
        rankFullIters[job] = dict()
        defRankFullIters[job] = dict()
      for rank in timing.rankAlgIters[job]:
        if rank not in rankAlgIters[job]:
          rankAlgIters[job][rank] = 0
          rankFullIters[job][rank] = 0
          defRankFullIters[job][rank] = 0
        rankAlgIters[job][rank] += timing.rankAlgIters[job][rank]
        rankFullIters[job][rank] += timing.rankFullIters[job][rank]
        defRankFullIters[job][rank] += timing.defRankFullIters[job][rank]
  for job in rankAlgIters:
    for rank in rankAlgIters[job]:
      rankAlgIters[job][rank] = rankAlgIters[job][rank] / len(timings)
      rankFullIters[job][rank] = rankFullIters[job][rank] / len(timings)
      defRankFullIters[job][rank] = defRankFullIters[job][rank] / len(timings)
  newTiming = TestTiming(timings[0].test, rankAlgIters, rankFullIters, defRankFullIters)
  newTiming.calcSpeedupFrom(newTiming)
  return newTiming

def getIsolatedName(trace, version, jobNum, numJobs, machine, rpn, placement, rr):
  name = [trace.name, str(version), str(jobNum) + "of" + str(numJobs), machine, str(rpn), str(placement)]
  if rr:
    name += ["rr"]
  return "-".join(name)

def getIsolatedTests(test):
  # Add tests where each job runs in isolation
  newTests = []
  if len(test.traces) > 1: # No need for isolated tests if there's only one trace
    beginNode = 0
    for trace in test.traces:
      endNode = beginNode + (trace.ranks / test.rpn) - 1
      newTests.append(Test([trace], [100], None, "Isolated_Nodes%dto%d" % (beginNode, endNode), test.rpn, beginNode, endNode, test.nodes))
      beginNode = endNode + 1
  return newTests

def call(params):
  commandLine = " ".join(params)
  print "Running " + commandLine
  subprocess.call(commandLine, shell=True)

def call_no_wait(params):
  if DEBUG:
    params = ['echo'] + params
  commandLine = " ".join(params)
  print "Running " + commandLine + " in the background"
  return subprocess.Popen(commandLine, shell=True)

# Creates the mapping file that tracer uses to place jobs
def make_mapping(test, seed, machine):
  call([os.path.join(SCRIPT_DIR, 'scripts', 'make_hosts.py'), '-n', str(test.totalNodes), '-r', str(test.rpn),
      '-x', '-s', str(seed), '-b', str(test.beginNode), '-e', str(test.endNode), '-p', machine])
  jobRanks = ','.join([str(trace.ranks) for trace in test.traces])
  call([os.path.join(SCRIPT_DIR, 'scripts', 'mapping.py'), '-p', jobRanks, '-r', str(test.rpn), '-s', machine])
  # Clean up
  for file in os.listdir('.'):
    if file.endswith('.txt') and file != "hosts.txt":
      remove(file)
    if file.startswith('global') and file != "global.bin":
      remove(file)

def getSLString(serviceLevels):
  return '"' + '", "'.join([str(sl) for sl in serviceLevels]) + '"'

def make_machine_config(test, machine):
  call(["sed", "'s/<prios>/( %s )/'" % getSLString(test.serviceLevels),
        "<", os.path.join(SCRIPT_DIR, 'machine-configs', '{0}.conf.template'.format(machine)), ">", "{0}.conf".format(machine)])
  call(["sed", "-i", "'s=<routing_dir>=%s='" % os.path.join(SCRIPT_DIR, "routing-tables", "{0}_routes".format(machine)), "{0}.conf".format(machine)])

def make_qos_mappings(test):
  for i in range(len(test.traces)):
    with open("qos%d.txt" % i, "w") as f:
      f.write("%d\n" % (test.numSLs - 1)) # Last SL (which is low priority) is the default
      for assignment in test.slAssignments[i]:
        f.write("%d %d\n" % (assignment.rank, assignment.SL))

def make_tracer_config(test, topDir):
  with open('tracer_config', 'w') as f:
    f.write('{0}\n'.format(os.path.join(topDir, 'global.bin')))
    f.write(str(test.numJobs) + '\n')
    for i in range(test.numJobs):
      traceFile = os.path.join(test.traces[i].tracePrefix + str(test.traceVersions[i]), 'traces.otf2')
      jobSpec = "%s %s%d %d 1 qos%d.txt" % (
          traceFile,
          os.path.join(topDir, 'job'),
          i,
          test.traces[i].ranks,
          i)
      f.write(jobSpec + '\n')
    #f.write('E 0 user_code 0.0\n')

def run_tracer(test, machine, node):
  printTime("Starting tracer for " + test.name)
  outFile = test.shortName + ".out"
  commandLine = [os.path.join(os.environ['HOME'], 'tracer', 'TraceR', 'bin', 'traceR'), '--sync=1',
    '--nkp=1', '--', '{0}.conf'.format(machine), 'tracer_config']
  if IS_MOAB:
    commandLine = ["srun", "-N1", '--nodelist=%s' % node, '--output=%s' % outFile] + commandLine
  else:
    commandLine = ["mpirun", "-np", "1"] + commandLine + [">", outFile]
  return call_no_wait(commandLine)

# Runs a test in tracer
def runTestInTracer(test, seed, machine, node, topDir):
  printTime("Starting test " + test.name + ", seed %d" % seed)
  make_machine_config(test, machine)
  make_qos_mappings(test)
  make_tracer_config(test, topDir)
  return run_tracer(test, machine, node)

def runTestInDir(test, seed, runDir, machine, node, topDir):
  mkdir(runDir)
  chdir(runDir)
  proc = runTestInTracer(test, seed, machine, node, topDir)
  chdir('..')
  return proc

def getDefaultTest(traces, rpn):
  return Test(traces, [10], None, "Default", rpn)

def runTest(test, seed, runDir, machine, node, iteration, topDir):
  defaultTest = getDefaultTest(test.traces, test.rpn)
  for i in range(test.numJobs):
    if test.traces[i].randomVersion:
      test.traceVersions[i] = random.randrange(0, test.traces[i].numVersions)
    else:
      test.traceVersions[i] = iteration
    defaultTest.traceVersions[i] = test.traceVersions[i]
  procs = []
  # Run the actual test
  procs.append(runTestInDir(test, seed, runDir, machine, node, topDir))
  # Run the corresponding default test
  procs.append(runTestInDir(defaultTest, seed, runDir + "-default", machine, node, topDir))
  for proc in procs:
    proc.wait()
  # Clean up
  remove(os.path.join(runDir, 'ross.csv'))
  remove(os.path.join(runDir + "-default", "ross.csv"))

  # Calculate the results
  (algIters, fullIters) = getRankIters(runDir, test.shortName)
  (_, defFullIters) = getRankIters(runDir + "-default", defaultTest.shortName)
  newTiming = TestTiming(test, algIters, fullIters, defFullIters)
  return newTiming

def getRankIters(directory, name):
  stopIterTime = 0.06

  stopIters = dict()
  with open(os.path.join(directory, name + ".out"), 'r') as input:
    for line in input:
      if "End TRACER_WallTime_Iteration" in line:
        tokens = line.split()
        time = float(tokens[6])
        if time < stopIterTime:
          job = int(tokens[1])
          rank = int(tokens[2])
          iterNum = int(tokens[5][len("TRACER_WallTime_Iteration"):])
          if job not in stopIters:
            stopIters[job] = dict()
          stopIters[job][rank] = (iterNum, time)

  fullIters = dict()
  for job in stopIters:
    fullIters[job] = dict()
    for rank in stopIters[job]:
      stop = stopIters[job][rank]

      fullItersPerSec = float(stop[0] + 1) / stop[1]
      # Calculate fractional iters completed
      fullIters[job][rank] = fullItersPerSec * stopIterTime
     
  return fullIters, fullIters

class GlobalRank:
  def __init__(self, job, rank):
    self.job = job
    self.rank = rank

  def __eq__(self, other):
    return self.job == other.job and self.rank == other.rank

  def __ne__(self, other):
    return not self.__eq__(other)

  def __hash__(self):
    return hash((self.job, self.rank))

  def __repr__(self):
    return str(self.job) + "-" + str(self.rank)

class OnLineAlgorithm:
  DEFAULT = 1
  RUNNING = 2
  SLEEPING = 3

  def __init__(self, numSls, candidateThreshold, candidatesPerJob, badJobThreshold, numDefaults):
    self.default = None
    self.defaults = []
    self.best = None
    self.numSls = numSls
    self.candidateThreshold = candidateThreshold
    self.candidatesPerJob = candidatesPerJob
    self.badJobThreshold = badJobThreshold
    self.numDefaults = numDefaults
    self.state = OnLineAlgorithm.DEFAULT

  def jobSetChange(self):
    # The set of jobs changed, so start the algorithm over
    self.default = None
    self.defaults = []
    self.best = None
    self.state = OnLineAlgorithm.DEFAULT

  def getCandidates(self):
    self.candidates = set()

    # Get the worst rank from every job
    for job in range(self.best.test.numJobs):
      jobIters = self.best.jobAlgIters[job]
      self.addCandidates(job, jobIters * (1.0 / self.candidateThreshold))

    print "Candidates: " + str(self.candidates)

  def addCandidates(self, job, threshold):
    rankIters = self.best.rankAlgIters[job]
    maxCandidates = round(len(rankIters) * self.candidatesPerJob)
    if maxCandidates < 1:
      maxCandidates = 1
    itersAndIndexes = [(rankIters[i], i) for i in range(len(rankIters))]
    itersAndIndexes.sort()
    newCandidates = 0
    for (iters, rank) in itersAndIndexes:
      if iters <= threshold:
        self.candidates.add(GlobalRank(job, rank))
        newCandidates += 1
        if newCandidates >= maxCandidates:
          break

  def selectCandidate(self):
    return self.candidates.pop()

  def copyBestSls(self):
    sls = copy.copy(self.best.test.serviceLevels)
    slAssignments = copy.deepcopy(self.best.test.slAssignments)
    return (sls, slAssignments)

  def getNewSls(self):
    if self.state == OnLineAlgorithm.DEFAULT:
      sls = [10]
      slAssignments = None
      return (sls, slAssignments)
    elif self.state == OnLineAlgorithm.RUNNING:
      rank = self.selectCandidate()
      return self.increasePriority(rank)
    else: # The algorithm has finished
      return self.copyBestSls()

  def increasePriority(self, rank):
    (sls, slAssignments) = self.copyBestSls()

    # See if this rank has been sped up before
    newAssignment = None
    for assignment in slAssignments[rank.job]:
      if assignment.rank == rank.rank:
        newAssignment = assignment
        break

    if newAssignment:
      # This rank was already sped up so speed it up more
      sls[assignment.SL] *= 2
    else:
      # This rank has not been sped up so create a new SL for it
      sls[-1] = 10
      sls.append(10)
      slAssignments[rank.job].append(SLAssignment(rank.rank, len(sls) - 2))

    return (sls, slAssignments)

  def recordTest(self, newTiming):
    if self.state == OnLineAlgorithm.DEFAULT:
      # We just ran a default test, so save it
      self.defaults.append(newTiming)
      action = "default"
      if len(self.defaults) >= self.numDefaults:
        self.best = newTiming
        self.default = averageTiming(self.defaults)
        print "Average default timing:"
        print "Rank alg iters: " + str(self.default.rankAlgIters)
        print "Rank full iters: " + str(self.default.rankFullIters)
        print "Default rank full iters: " + str(self.default.defRankFullIters)
        self.getCandidates()
        if len(self.candidates) > 0:
          self.state = OnLineAlgorithm.RUNNING
        else:
          self.state = OnLineAlgorithm.SLEEPING
    elif self.state == OnLineAlgorithm.SLEEPING:
      if newTiming.overallAlgSpeedup < 0:
        self.best = None
        self.default = None
        self.defaults = []
        self.state = OnLineAlgorithm.DEFAULT
        action = "restart"
      else:
        action = "sleeping"
    else:
      if newTiming.overallAlgSpeedup > self.best.overallAlgSpeedup and all(sp > self.badJobThreshold for sp in newTiming.jobAlgSpeedups):
        # We have a new best so create a new candidate list
        self.best = newTiming
        # Since we've changed the service level we need to calculate new candidates
        self.getCandidates()
        action = "accepted"
      else:
        action = "rejected"
      if len(self.best.test.serviceLevels) >= self.numSls or len(self.candidates) == 0:
        self.state = OnLineAlgorithm.SLEEPING
    return action

def getNNRowSize(traces):
  # Make sure all nearest neighbor jobs have the same row size
  rowSize = 0
  for trace in traces:
    if trace.nnRowSize > 0:
      if rowSize == 0:
        rowSize = trace.nnRowSize
      else:
        assert trace.nnRowSize == rowSize, "Mismatch in nearest neighbor row sizes"
  return rowSize

class TestRecorder():
  def __init__(self, traces):
    self.outFile = open('results.csv', 'w')
    self.writer = csv.writer(self.outFile, dialect=csv.excel)
    self.numTraces = len(traces)
    self.defSums = [0 for i in range(self.numTraces)]
    self.qosSums = [0 for i in range(self.numTraces)]
    self.isoSums = [0 for i in range(self.numTraces)]
    header = ['Iteration']
    for i in range(self.numTraces):
      header += ['Trace {0} Number'.format(i), str(i) + '-' + traces[i].name + " Alg Iters",
          "Full Iters", "Default Iters", "Isolated Iters",
          'Alg Speedup', 'Speedup From Default',
          'Default Speedup from Isolated', 'Speedup from Isolated']
    header += ['Average Alg Speedup',
        'Average Speedup from Default', 'Default Speedup from Isolated',
        'Average Speedup from Isolated', 'State', 'Job Change']
    self.writer.writerow(header)

  def recordTest(self, iteration, testTiming, isolated, state, doJobChange):
    row = [iteration]
    isoSpeedupSum = 0
    defSpeedupSum = 0
    for i in range(self.numTraces):
      iso = isolated[(testTiming.test.traces[i].name,i)][testTiming.test.traceVersions[i]]
      isoSpeedup = speedup(iso, testTiming.jobFullIters[i])
      isoSpeedupSum += isoSpeedup
      defSpeedup = speedup(iso, testTiming.defJobFullIters[i])
      defSpeedupSum += defSpeedup

      self.defSums[i] += testTiming.defJobFullIters[i]
      self.qosSums[i] += testTiming.jobFullIters[i]
      self.isoSums[i] += iso

      row += [str(testTiming.test.traceVersions[i]), str(testTiming.jobAlgIters[i]), 
          str(testTiming.jobFullIters[i]), str(testTiming.defJobFullIters[i]), str(iso),
          str(testTiming.jobAlgSpeedups[i] * 100), str(testTiming.jobFullSpeedups[i] * 100),
          str(defSpeedup * 100), str(isoSpeedup * 100)]
    row += [str(testTiming.overallAlgSpeedup * 100),
        str(testTiming.overallFullSpeedup * 100),
        str(defSpeedupSum * 100 / self.numTraces),
        str(isoSpeedupSum * 100 / self.numTraces), state, doJobChange]
    self.writer.writerow(row)
    self.outFile.flush()

  def close(self):
    row = ['Totals']
    deToQosSum = 0
    isoToDeSum = 0
    isoToQosSum = 0
    for i in range(self.numTraces):
      qos = self.qosSums[i]
      de  = self.defSums[i]
      iso = self.isoSums[i]
      deToQos = speedup(de, qos)
      isoToDe = speedup(iso, de)
      isoToQos = speedup(iso, qos)
      deToQosSum += deToQos
      isoToDeSum += isoToDe
      isoToQosSum += isoToQos
      row += ['', '', str(qos), str(de), str(iso),
          '', str(deToQos * 100), str(isoToDe * 100), str(isoToQos * 100)]
    row += ['', str(deToQosSum * 100 / self.numTraces),
        str(isoToDeSum * 100 / self.numTraces),
        str(isoToQosSum * 100 / self.numTraces), '']
    self.writer.writerow(row)
    self.outFile.close()

def findTrace(name):
  for trace in allTraces:
    if name == trace.name:
      return trace
  return None

def loadIsolated(trace, jobNum, numJobs, args, isolated):
  key = (trace.name, jobNum)
  isolated[key] = dict()
  for version in range(trace.numVersions):
    name = getIsolatedName(trace, version, jobNum, numJobs, args.machine, args.rpn, args.seed, args.rr)
    (_, fullIters) = getRankIters(TRACES_DIR, name)
    jobIters = calcJobIters(fullIters)
    isolated[key][version] = jobIters[0]

def runQoSAlgorithm(traces, args):

  printTime("Start time")

  currDir = os.getcwd()
  resultsDir = currDir

  dummy = getDefaultTest(traces, args.rpn) # Dummy test - used to write the mapping file
  make_mapping(dummy, args.seed, args.machine)
  recorder = TestRecorder(traces)

  # Load the isolated tests
  isolated = dict()
  numJobs = len(traces)
  for jobNum in range(numJobs):
    loadIsolated(traces[jobNum], jobNum, numJobs, args, isolated)
    if args.arrivals:
      altTrace = findTrace(traces[jobNum].name.replace("50", "25"))
      loadIsolated(altTrace, jobNum, numJobs, args, isolated)

  if args.arrivals:
    numIters = NUM_ITERATIONS * 5
  else:
    numIters = NUM_ITERATIONS

  # Run the algorithm
  alg = OnLineAlgorithm(args.numSls, args.candidateThreshold, args.candidatesPerJob, args.badJobThreshold, args.defaults)
  for iteration in range(numIters):
    testDir = str(iteration)
    printTime("Starting iteration " + testDir)
    doJobChange = args.arrivals and iteration > 0 and iteration % NUM_ITERATIONS == 0
    if doJobChange:
      # Time for a job departure and arrival
      alg.jobSetChange()
      idx = (int(iteration / NUM_ITERATIONS) - 1) * int(len(traces) / 4)
      name = traces[idx].name
      traces[idx] = findTrace(name.replace("50", "25"))
    (sls, slAssignments) = alg.getNewSls()
    print "Service levels: {0}".format(sls)
    print "Assignments: {0}".format(slAssignments)
    newTest = Test(traces, sls, slAssignments, "Simple", args.rpn)
    newTiming = runTest(newTest, args.seed, testDir, args.machine, args.node, iteration, "..")
    newTiming.calcSpeedupFrom(alg.default)
    newTiming.printSpeedup()
    state = alg.recordTest(newTiming)
    recorder.recordTest(testDir, newTiming, isolated, state, doJobChange)
    iteration += 1

  print "Best speedup:"
  alg.best.printSpeedup()

  recorder.close()
  chdir(currDir)
  print("Output was written to: " + resultsDir)
  print "Done."
  printTime("End time")

def runIsolated(traces, args):
  printTime("Running isolated tests")
  defaultTest = getDefaultTest(traces, args.rpn)
  tests = getIsolatedTests(defaultTest)
  numVersions = max(trace.numVersions for trace in traces)
  procs = []
  numJobs = len(traces)
  for version in range(numVersions):
    mkdir(str(version))
    chdir(str(version))
    for test in tests:
      if version < test.traces[0].numVersions:
        test.traceVersions[0] = version
        mkdir(test.shortName)
        chdir(test.shortName)
        make_mapping(test, args.seed, args.machine)
        procs.append(runTestInTracer(test, args.seed, args.machine, args.node, "."))
        chdir('..')
        if len(procs) >= SIMS_PER_NODE:
          for proc in procs:
            proc.wait()
          procs = []
    chdir('..')
  for proc in procs:
    proc.wait()

  # Copy the isolated tests to a permanent location so we can use them for future runs
  for version in range(numVersions):
    chdir(str(version))
    for jobNum in range(numJobs):
      test = tests[jobNum]
      if version < test.traces[0].numVersions:
        src = os.path.join(test.shortName, test.shortName + ".out")
        dest = os.path.join(TRACES_DIR, getIsolatedName(test.traces[0], version, jobNum, numJobs, args.machine, args.rpn, args.seed, args.rr)) + ".out"
        print "Copying {0} to {1}".format(src, dest)
        shutil.copy(src, dest)
    chdir('..')

  printTime("Done")

def parseBool(value):
  return value == "True"

def execute():
  parser = argparse.ArgumentParser(description="Test runner for QoS tests")
  parser.add_argument('node', help="The node to run on")
  parser.add_argument('operation', choices=['qos', 'isolated'], help="The operation to run")
  parser.add_argument('traces', help="The traces to run")
  parser.add_argument('machine', help="The machine to simulate")
  parser.add_argument('rpn', type=int, help="Ranks per node")
  parser.add_argument('numSls', type=int, help="The number of available service levels")
  parser.add_argument('seed', type=int, help="The seed for randomly placing nodes")
  parser.add_argument('candidateThreshold', type=float, help="Ranks within this percent of the worst job will be considered as candidates")
  parser.add_argument('candidatesPerJob', type=float, help="Up to this percent of the total ranks in each job will be considered as candidates")
  parser.add_argument('badJobThreshold', type=float, help="Solutions in which any job has a worse slowdown than this will be rejected")
  parser.add_argument('defaults', type=int, help="The number of iterations to average for the default")
  parser.add_argument('--arrivals', action='store_true', help="Whether to simulate job arrivals and departures")
  parser.add_argument('--rr', action='store_true', help="Whether to use round-robin isolated results (the default is fcfs)")
  args = parser.parse_args()

  # Set the random seed for selecting traces
  random.seed(TRACE_SEED)

  print "Arguments:"
  print args

  # Parse the traces
  traces = []
  traceNames = args.traces.split(',')
  for traceName in traceNames:
    found = False
    if args.arrivals:
      assert "50" in traceName, "Job arrivals only supported when switching from c50 jobs to c25 jobs"
      assert findTrace(traceName.replace("50", "25")), "Could not find trace to switch to for " + traceName
    trace = findTrace(traceName)
    assert trace, "Could not find trace " + traceName
    trace.checkParams()
    traces.append(trace)

  if args.operation == "qos":
    runQoSAlgorithm(traces, args)
  elif args.operation == "isolated":
    runIsolated(traces, args)

execute()
