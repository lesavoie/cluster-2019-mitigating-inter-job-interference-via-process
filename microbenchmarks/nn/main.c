/*
 * Simple 2D nearest neighbor benchmark
 *
 * To do:
 * -Time only the communication loop
 */


extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#ifdef SCOREP_USER_ENABLE
#include <scorep/SCOREP_User.h>
#endif

#define TAG 0

#define NN_DEBUG

void postRecv(int *, int, int, MPI_Request [], int[], int *);
void postSend(int *, int, int, MPI_Request [], int[], int *);

int rand_range(int M, int N) {
  return M + rand() / (RAND_MAX / (N - M + 1) + 1);
}

MPI_Comm appWorld;

int nn_main(int argc, char ** argv, MPI_Comm comm) {

appWorld = comm;

int rank, ranks;
MPI_Comm_size(appWorld, &ranks);
MPI_Comm_rank(appWorld, &rank);

if (argc < 8) {
  if (rank == 0) printf("Usage: %s <message size> <iterations> <phases> <comp time per iter> <comp imbalance> <random seed> <row size>\n", argv[0]);
  MPI_Abort(MPI_COMM_WORLD, 1);
}
int msgSize, iterations, numPhases, maxSleepTime, sleepTime = 0, compImbalance, seed, rowSize, paradisIter, sleepMod;
msgSize = atoi(argv[1]);
iterations = atoi(argv[2]);
numPhases = atoi(argv[3]);
maxSleepTime = atoi(argv[4]);
compImbalance = atoi(argv[5]);
seed = atoi(argv[6]);
rowSize = atoi(argv[7]);
if (argc >= 9) paradisIter = atoi(argv[8]);
else paradisIter = -1;
sleepMod = -1;

srand(rank + seed);
char imbalanceDesc[10];
switch(compImbalance) {
  case 1: // Small
    sleepTime = rand_range(maxSleepTime * 0.9, maxSleepTime);
    strcpy(imbalanceDesc, "Small");
    break;
  case 2: // Medium
    sleepTime = rand_range(0, maxSleepTime);
    strcpy(imbalanceDesc, "Medium");
    break;
  case 3: // Large
    if (rank == 0) sleepTime = maxSleepTime;
    else sleepTime = 0;
    strcpy(imbalanceDesc, "Large");
    break;
  case 4: // ParaDiS like
    if (argc < 9) {
      if (rank == 0) printf("Must provide ParaDiS iter with comp imbalance = 4\n");
      MPI_Abort(MPI_COMM_WORLD, 2);
    }
    int maxIncrement = maxSleepTime / 250;
    sleepMod = rand_range(-maxIncrement, maxIncrement);
    sleepTime = maxSleepTime + (sleepMod * paradisIter);
    strcpy(imbalanceDesc, "ParaDiS");
    break;
  default:
    if (rank == 0) printf("Invalid computation imbalance. Valid values are 1 (small), 2 (medium), and 3 large)\n");
    MPI_Abort(MPI_COMM_WORLD, 3);
    break;
}

#ifdef NN_DEBUG
if (rank == 0) {
  printf("Message size: %d bytes\n", msgSize);
  printf("Iterations: %d\n", iterations);
  printf("Phases: %d\n", numPhases);
  printf("Max computation time: %d ns\n", maxSleepTime);
  printf("Imbalance: %s\n", imbalanceDesc);
  printf("Random seed: %d\n", seed);
  printf("Row size: %d\n", rowSize);
  if (argc >= 9) {
    printf("ParaDiS iter: %d\n", paradisIter);
  }
}
if (argc >= 9) {
  printf("ParaDiS sleep time mod: %d\n", sleepMod);
}
printf("Rank %d computation time: %d ns\n", rank, sleepTime);
fflush(stdout);
#endif

// Calculate the size of the columns
if (ranks % rowSize != 0) {
  if (rank == 0) printf("Row size must divide evenly into the number of ranks.\n");
  MPI_Abort(appWorld, 4);
}
int colSize = ranks / rowSize;
msgSize /= 4; // Convert from bytes to ints since that's what we'll be sending

// Separate the rank from the position in the grid so that it can be customized as needed
int * pos_to_rank = (int *)malloc(sizeof(int) * ranks);
int pos = rank;
int j;
for (j = 0; j < ranks; ++j) {
  pos_to_rank[j] = j;
}

for (j = 0; j < ranks; ++j) {
  if (pos_to_rank[j] == rank) {
    pos = j;
    break;
  }
}


// Find my neighbors
int myX, myY;
int leftN, topN, rightN, bottomN, neighbors;
leftN = topN = rightN = bottomN = -1;
neighbors = 0;
myX = pos % rowSize;
myY = pos / rowSize;
if (myX > 0) {
  leftN = pos - 1;
  ++neighbors;
}
if (myX < rowSize - 1) {
  rightN = pos + 1;
  ++neighbors;
}
if (myY > 0) {
  topN = pos - rowSize;
  ++neighbors;
}
if (myY < colSize - 1) {
  bottomN = pos + rowSize;
  ++neighbors;
}
//printf("Rank %d neighbors: left %d, top %d, right %d, bottom %d\n", rank, leftN, topN, rightN, bottomN);

// Communicate with neighbors
int leftSendBuf[msgSize], topSendBuf[msgSize], rightSendBuf[msgSize], bottomSendBuf[msgSize];
int leftRecvBuf[msgSize], topRecvBuf[msgSize], rightRecvBuf[msgSize], bottomRecvBuf[msgSize];
MPI_Request requests[neighbors * 2];

#ifdef PROFILE
MPI_Barrier(appWorld);
initProfile(numPhases);
#endif

int phases = 0;
int iters = 0;
double startstep, endstep;

int toTime = numPhases * iterations / 3;

SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_MainLoop", SCOREP_USER_REGION_TYPE_COMMON);
SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_ForTiming", SCOREP_USER_REGION_TYPE_COMMON);
double startt = MPI_Wtime();
int i;
for (i = 0; i < numPhases * iterations; ++i) {
  char region_name[30];
  sprintf(region_name, "TRACER_WallTime_Iteration%d", i);

  startstep = MPI_Wtime();
  SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);

  // "Computation" for the phase
  if (sleepTime) {
#ifdef PROFILE
    startComp();
#endif

    do_sleep(sleepTime);

#ifdef PROFILE
    endComp();
#endif
  }

#ifdef PROFILE
  startCommExchange();
#endif

  // Communication
  int reqi = 0;

  postRecv(leftRecvBuf, msgSize, leftN, requests, pos_to_rank, &reqi);
  postRecv(topRecvBuf, msgSize, topN, requests, pos_to_rank, &reqi);
  postRecv(rightRecvBuf, msgSize, rightN, requests, pos_to_rank, &reqi);
  postRecv(bottomRecvBuf, msgSize, bottomN, requests, pos_to_rank, &reqi);

  postSend(leftSendBuf, msgSize, leftN, requests, pos_to_rank, &reqi);
  postSend(topSendBuf, msgSize, topN, requests, pos_to_rank, &reqi);
  postSend(rightSendBuf, msgSize, rightN, requests, pos_to_rank, &reqi);
  postSend(bottomSendBuf, msgSize, bottomN, requests, pos_to_rank, &reqi);

  MPI_Waitall(reqi, requests, MPI_STATUSES_IGNORE);

  SCOREP_USER_REGION_BY_NAME_END(region_name);
  endstep = MPI_Wtime();
  printf("Rank %d, position %d, iteration %d, iteration time: %f\n", rank, pos, i, endstep - startstep);
  ++iters;
  if (i == toTime) SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_ForTiming");

#ifdef PROFILE
  endCommExchange();
#endif

  // Synchronize every "iterations" timesteps.
  if ((i + 1) % iterations == 0) {
#ifdef PROFILE
    enterSync();
#endif
    MPI_Barrier(appWorld);
    ++phases;
#ifdef PROFILE
    exitSync(rank);
#endif
  }

}

// End app timing.
double endt = MPI_Wtime();
double myTime = endt - startt;

SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_MainLoop");
MPI_Barrier(appWorld);

#ifdef SCOREP_USER_ENABLE
if (SCOREP_RECORDING_IS_ON()) {
  SCOREP_RECORDING_OFF();
}
#endif

// Gather the running times
double maxTime;
MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
if (rank == 0) {
  printf("Execution time: %f\n", maxTime);
}

#ifdef PROFILE
writeProfile(rank, ranks);
#endif

print_hosts();
return 0;

}

void postRecv(int * buf, int size, int source, MPI_Request requests[], int pos_to_rank[], int * reqi) {
  if (source >= 0) {
    MPI_Irecv(buf, size, MPI_INT, pos_to_rank[source], TAG, appWorld, &requests[*reqi]);
    ++(*reqi);
  }
}

void postSend(int * buf, int size, int dest, MPI_Request requests[], int pos_to_rank[], int * reqi) {
  if (dest >= 0) {
    MPI_Isend(buf, size, MPI_INT, pos_to_rank[dest], TAG, appWorld, &requests[*reqi]);
    ++(*reqi);
  }
}
