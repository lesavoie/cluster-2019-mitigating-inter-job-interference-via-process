#!/bin/bash

#MSUB -q pbatch
#MSUB -l nodes=20
#MSUB -l partition=catalyst
#MSUB -l walltime=00:20:00
#MSUB -V
#MSUB -m ae

NODES=20
RANKS=$[$NODES * 1]
MSG_SIZE=$[64 * 1024 * 16]
ITERS=36
PHASES=1
SLEEP=1500000
IMBALANCE=2 # 1=small, 2=med, 3=large, 4=ParaDiS
SEED=2
ROW_SIZE=4
PARADIS_ITER=0

export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=163840000

date
srun -l -N $NODES -n $RANKS ~/benchmarks/nn/nn $MSG_SIZE $ITERS $PHASES $SLEEP $IMBALANCE $SEED $ROW_SIZE $PARADIS_ITER
date
