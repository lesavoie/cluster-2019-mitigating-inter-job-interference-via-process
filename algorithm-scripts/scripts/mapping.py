#!/usr/bin/env python

import argparse
import os
import sys

scriptPath = os.path.abspath(os.path.dirname(__file__))

class System():
  def __init__(self, name, cores_per_node, nodes_per_router):
    self.name = name
    self.cores_per_node = cores_per_node
    self.nodes_per_router = nodes_per_router

# Translates a host id and core id
# to the CODES global processor id.
# Example: (cab1, 0) -> LP id for first core on first node
def get_global_id(host, coreid, system):
  # catalystXXX => XXX with zero-based indexing
  hostid = int(host[len(system.name):]) - 1
  # get router and node within router
  router = hostid / system.nodes_per_router
  node_in_router = hostid % system.nodes_per_router

  # work out global id within router
  id_in_router = coreid * system.nodes_per_router + node_in_router

  # work out full global id
  globalid = id_in_router + router * system.cores_per_node * system.nodes_per_router

  return globalid, ','.join([host, str(coreid)])



if __name__ == '__main__':
  systems = {}
  systems["small"] = System("small", 16, 8)
  systems["inter"] = System("inter", 16, 12)
  systems["catalyst"] = System("catalyst", 24, 18)
  systems["cab"] = System("cab", 16, 18)
  systems["medium"] = System("medium", 24, 18)

  parser = argparse.ArgumentParser(description="Build mapping files for TraceR")
  jobGroup = parser.add_mutually_exclusive_group()
  jobGroup.add_argument("-j", "--numjobs", type=int, help="Number of jobs to build mapping files for")
  jobGroup.add_argument("-p", "--ranks_per_job", help="Comma separated list of the number of ranks for each job")
  parser.add_argument("-r", "--rpn", type=int, help="Number of ranks to assign to each node")
  parser.add_argument("-f", "--host_file", default="hosts.txt", help="File to read hosts from")
  parser.add_argument("-s", "--system", choices=list(systems.keys()), default="small", help="The system to simulate")
  args = parser.parse_args()


  # read hosts file, node id for each rank, one per line
  f = open(args.host_file, 'r')
  hosts = f.readlines()
  f.close()

  # command line arguments
  if args.numjobs:
    numjobs = args.numjobs
    ranks_per_job = len(hosts) / numjobs
    ranks = [ranks_per_job] * numjobs
  else:
    ranks = [int(r) for r in args.ranks_per_job.split(',')]
    numjobs = len(ranks)
  rpn = args.rpn

  # open all output files
  globalout = open('global.txt', 'w')
  globaljobs = []
  jobouts = []

  for i in range(numjobs) :
    globaljob = open('global%d.txt' % i, 'w')
    globaljobs.append(globaljob)
    jobout = open('job%d.txt' % i, 'w')
    jobouts.append(jobout)

  coreid = 0
  host = hosts[0].rstrip()

  rank = 0
  job = 0

  for line in hosts:
    if rank == ranks[job]:
      # reset for next job
      rank = 0
      job += 1

    nexthost = line.rstrip()
    if nexthost != host:
      # sanity check
      if coreid != rpn:
        print 'Warning: rpn argument does not match host file'
      # reset coreid for next node
      coreid = 0
      host = nexthost

    # get the global processor id for CODES
    globalid, info = get_global_id(host, coreid, systems[args.system])
    #print globalid, info

    # write to the appropriate output files
    globalout.write('%d %d %d\n' % (globalid, rank, job))
    globaljobs[job].write('%d %d %d\n' % (globalid, rank, 0))
    jobouts[job].write('%d\n' % globalid)

    # next core and rank
    coreid += 1
    rank += 1

  # done, close up files
  globalout.close()
  for globaljob in globaljobs:
    globaljob.close()
  for jobout in jobouts:
    jobout.close()

  # call C code to write the final binary outputs
  from subprocess import call
  globaljob_names = map(lambda x: x.name, globaljobs)
  jobout_names = map(lambda x: x.name, jobouts)
  call([scriptPath + "/code_mapping/write_mapping", str(numjobs), globalout.name] + globaljob_names + jobout_names)

