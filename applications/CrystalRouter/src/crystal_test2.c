#define _GNU_SOURCE
#include <sched.h>

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "c99.h"
#include "name.h"
#include "fail.h"
#include "types.h"
#include "comm.h"
#include "mem.h"
#include "crystal.h"
#include "sort.h"
#include "sarray_sort.h"
#include "sarray_transfer.h"

#include <scorep/SCOREP_User.h>

void print_affinity(int myrank) {
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  sched_getaffinity(0, sizeof(cpu_set_t), &my_set);

  char str[80];
  strcpy(str," ");
  int count = 0;
  int j;
  for (j = 0; j < CPU_SETSIZE; j++) {
    if (CPU_ISSET(j, &my_set)) {
      count++;
      char cpunum[3];
      sprintf(cpunum, "%d ", j);
      strcat(str, cpunum);
    }
  }
  printf("Rank %d affinity has %d CPUs ... %s\n", myrank, count, str);
  fflush(stdout);
}

void set_affinity() {
  int rank, core;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  core = rank % 22 + 1;
  if (core >= 12) core += 1;
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  CPU_SET(core, &my_set);
  sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
  print_affinity(rank);
}


int main(int narg, char *arg[])
{
  comm_ext world; int np;
  struct comm comm;
  struct crystal cr;
  uint i,n;

  struct test { uint f; uint modf; };
  struct test *work1;
  struct array A;
  double r, ttime;

  //libdumpi_disable_profiling();
#ifdef MPI
  MPI_Init(&narg,&arg);
  //set_affinity();
  world = MPI_COMM_WORLD;
  MPI_Comm_size(world,&np);
#else
  world=0, np=1;
#endif

  comm_init(&comm,world);
  
  crystal_init(&cr,&comm);                      //copies comm and initialized arrays of cr

  uint size  = 1000;                            //number of messages per proc
  array_init(struct test,&A,size); 

  uint niter = 1000;                            //number of iterations/tests
  if (narg >= 2) {
    long n = strtol(arg[1], NULL, 10);
    if (n > 0 && n < INT_MAX) {
      niter = n;
    }
  }
  if (comm.id == 0) printf("Number of iterations: %d\n", niter);
  srand(time(0)+comm.id);                       //seed random number

  comm_barrier(&comm);
  double tstart=comm_time();                    //start clock
  char region_name[30];
  //libdumpi_enable_profiling();
  SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_MainLoop", SCOREP_USER_REGION_TYPE_COMMON);
  for(n=1;n<=niter;++n) {
    sprintf(region_name, "TRACER_WallTime_Iteration%d", n-1);
    SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);
    A.n   = size;
    work1 = A.ptr;
    for(i=0;i<A.n;++i)  {
          r = rand() * (1.0/(RAND_MAX+1.0));
          work1[i].f    =r*9999;                //random integer bound 
          work1[i].modf =work1[i].f % comm.np;  //mod of work1=target proc
      }

    sarray_sort(struct test,work1,A.n,modf,0,&cr.data);

    sarray_transfer(struct test,&A,modf,1,&cr);
    sarray_transfer(struct test,&A,modf,1,&cr);
    SCOREP_USER_REGION_BY_NAME_END(region_name);
  }
  SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_MainLoop");
  //libdumpi_disable_profiling();
  double tstop=comm_time();                       //end clock
  double etime = (tstop - tstart) ;
  ttime = etime/(double)niter;                    //divid by iterations
  ttime = ttime/2.0;                              //divid by transfer calls
 
  if(comm.id==0) {
    printf("Iteration time: %f\n", ttime);
    printf("Time elapsed: %f\n", etime);
  }

  array_free(&A);
  crystal_free(&cr);
  comm_barrier(&comm);
  comm_free(&comm);

  
#ifdef MPI
  MPI_Finalize();
#endif

  return 0;
}
