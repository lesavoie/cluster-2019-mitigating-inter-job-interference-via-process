#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "c99.h"
#include "name.h"
#include "fail.h"
#include "types.h"
#include "comm.h"
#include "mem.h"
#include "crystal.h"
#include "sort.h"
#include "sarray_sort.h"

int main(int narg, char *arg[])
{
  comm_ext world; int np;
  struct comm comm;
  struct crystal cr;
  uint i, *data, *end;

  struct test { uint f; uint modf; uint c; };
  buffer buf={0,0,0};
  uint count,j,j0;
  double r, ttime;

#ifdef MPI
  MPI_Init(&narg,&arg);
  world = MPI_COMM_WORLD;
  MPI_Comm_size(world,&np);
#else
  world=0, np=1;
#endif

  comm_init(&comm,world);
  
  crystal_init(&cr,&comm);                      //copies comm and initialized arrays of cr

  uint size  = 10;                            //number of messages per proc
  struct test work1[size];

  uint niter = 1;                             //number of iterations/tests
  srand(time(0)+comm.id);                       //seed random number

  double tstart=comm_time();                    //start clock
  for(int n=1;n<=niter;++n) {

    for(i=0;i<size;++i)  {
          r = rand() * (1.0/(RAND_MAX+1.0));
          work1[i].f    =r*99999;               //random integer bound 
          work1[i].modf =work1[i].f % comm.np;  //mod of work1=target proc
      }

  for(i=0;i<size;++i)
    printf("so far...%i %i\n",work1[i].f,work1[i].modf);
    sarray_sort(struct test,work1,size,modf,0,&buf);
  for(i=0;i<size;++i)
    printf("and then...%i %i\n",work1[i].f,work1[i].modf);
  
    count = 0, j = 0, j0 = 0;                   //count # to send to proc 
    for(i=0;i<size;++i){
       j=work1[i].modf;
       if(j==j0){ 
         count += 1; 
         work1[i].c=count;
       }
  
       if(j!=j0) { 
          work1[i-count].c=count;
          count  = 1, j0=j;
       }
    }
    work1[i-count].c=count;
//for(i=0;i<size;++i)
//  printf("finally...%i %i %i\n",work1[i].f,work1[i].modf,work1[i].c);

    cr.data.n = size+(3*comm.np);                 //number of integers in cr
    buffer_reserve(&cr.data,cr.data.n*sizeof(uint)); 
    data = cr.data.ptr;                           //starting address
    for(j=0,i=0;i<comm.np;++i, data+=3+data[2]) {    
      if(j>=size) {                               //for proc not sending
         data[0] = 0;
         data[1] = comm.id;
         data[2] = 0;
      }else {
        data[0] = work1[j].modf;
        data[1] = comm.id;
        data[2] = work1[j].c;
      for(int k=3; k<data[2]+3;++k,j++)
         data[k]=work1[j].f;
      }
//  printf("CHECK %i %i %i %i\n",data[0],data[1],data[2],j);
    }
//data=cr.data.ptr;
//for(i=0;i<cr.data.n;++i)
//   printf("WHAT %i %i \n",data[i],comm.id);

#if 1
  data = cr.data.ptr, end = data + cr.data.n;
  for(;data!=end; data+=3+data[2]) {
    uint i;
    printf("%u -> %u:",data[1],data[0]);
    for(i=0;i<data[2];++i) printf(" %u",data[3+i]);
    printf("\n");
  }
#endif
    crystal_router(&cr);
    crystal_router(&cr);
#if 1
  printf("\n");
  data = cr.data.ptr, end = data + cr.data.n;
  for(;data!=end; data+=3+data[2]) {
    uint i;
    printf("%u <- %u:",data[0],data[1]);
    for(i=0;i<data[2];++i) printf(" %u",data[3+i]);
    printf("\n");
  }
#endif
  }
  double tstop=comm_time();                       //end clock
  ttime = (tstop - tstart) ;
  ttime = ttime/(double)niter;                    //divid by iterations
 
  if(comm.id==0) printf("Time elapsed: %f\n",ttime);  

  crystal_free(&cr);
  buffer_free(&buf);
  comm_free(&comm);

//diagnostic("",__FILE__,__LINE__,
//  "test successful %u/%u",(unsigned)comm.id,(unsigned)comm.np);
  
#ifdef MPI
  MPI_Finalize();
#endif

  return 0;
}
