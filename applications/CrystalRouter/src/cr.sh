#!/bin/bash -x

#MSUB -q pbatch
#MSUB -l nodes=32
#MSUB -l partition=catalyst
#MSUB -l walltime=00:20:00
#MSUB -V
#MSUB -m ae

NODES=32
RANKS=$[$NODES * 1]
ITERATIONS=625

export SCOREP_ENABLE_TRACING=true

date
srun -l -N $NODES -n $RANKS ${HOME}/CrystalRouter/src/crystal_test2 $ITERATIONS
date
