This repository contains the code used in the CLUSTER 2019 short paper titled
"Mitigating Inter-Job Interference via Process-Level Quality-of-Service".

The algorithm-scripts directory contains the Python scripts mentioned in the
paper that implement the QoS algorithm. The top level script is called
runner.py; it has a built-in help function that describes its parameters.

The microbenchmarks directory contains the microbenchmarks used in the paper.
The Mini-ParaDiS benchmark is actually the nearest neighbor (nn) benchmark
with IMBALANCE set to 4.

The applications directory includes applications that were not included
in the paper but were used in follow up investigation. Not all applications
are included because of distribution restrictions.

The traces directory includes the traces used for the experiments presented
in the paper, plus some traces used in additional experiments.
