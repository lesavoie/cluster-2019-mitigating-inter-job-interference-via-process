#!/usr/bin/env python

import argparse
import random

parser = argparse.ArgumentParser(description="Create a hosts file that can be used by mapping.py")
parser.add_argument("-n", "--nodes", type=int, default=32, help="The number of nodes to include")
parser.add_argument("-r", "--rpn", type=int, default=16, help="The number of ranks per node")
parser.add_argument("-p", "--prefix", default="small", help="String to include at the beginning of all node names")
parser.add_argument("-x", "--random", action="store_true", help="Whether to randomize node placements")
parser.add_argument("-s", "--seed", type=int, default=0, help="The seed for the random number generator")
parser.add_argument("-b", "--begin", type=int, default=0, help="The zero-based index of the first node to include")
parser.add_argument("-e", "--end", type=int, default=-1, help="The zero-based index of the last node to include")
args = parser.parse_args()

NODES = args.nodes
RANKS_PER_NODE = args.rpn
NAME = args.prefix
BEGIN = args.begin
END = args.end
if END < 0:
  END=NODES-1

random.seed(args.seed)

with open('hosts.txt', 'w') as f:
  ranks = range(0, NODES)
  if args.random:
    random.shuffle(ranks)
  for (i, counter) in zip(ranks, range(0, NODES)):
    if counter >= BEGIN and counter <= END:
      name = "%s%d" % (NAME, i+1)
      for j in range(0, RANKS_PER_NODE):
        f.write(name + "\n")
