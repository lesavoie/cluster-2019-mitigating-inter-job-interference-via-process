#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef SCOREP_USER_ENABLE
#include <scorep/SCOREP_User.h>
#endif

#define NN_DEBUG

extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

MPI_Comm appWorld;

int rand_range(int M, int N) {
  return M + rand() / (RAND_MAX / (N - M + 1) + 1);
}

/*
 * Performs communication between N/2 random pairs of nodes.
 */
void p2p(MPI_Comm comm, int numprocs, int rank, int msgSize, int num_time_steps, int iterations, long sleep) {
  // Determine "random" partners first.
  int *partners = malloc(numprocs / 2 * sizeof(int));
  if (rank == 0) {
    int i;
    for (i = 0; i < numprocs / 2; i++) {
      partners[i] = numprocs / 2 + i;
    }

    srand(1331);
    for (i = 0; i < 2 * numprocs; i++) {
      int index1 = rand() % (numprocs / 2);
      int index2 = rand() % (numprocs / 2);
      int temp = partners[index1];
      partners[index1] = partners[index2];
      partners[index2] = temp;
    }
  }
  MPI_Bcast(partners, numprocs / 2, MPI_INT, 0, comm);

  // Allocate message space and send the messages.
  int *sendBuf = malloc(msgSize);
  int *recvBuf = malloc(msgSize);

#ifdef PROFILE
  MPI_Barrier(comm);
  initProfile(num_time_steps);
#endif

  // Start timing
  double startt = MPI_Wtime();
  int i;
  int toTime = (num_time_steps * iterations) / 3;
  printf("Iterations to time: %d\n", toTime);

  SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_MainLoop", SCOREP_USER_REGION_TYPE_COMMON);
  SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_ForTiming", SCOREP_USER_REGION_TYPE_COMMON);
  for(i = 0; i < num_time_steps * iterations; i++){
    char region_name[30];
    sprintf(region_name, "TRACER_WallTime_Iteration%d", i);
    SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);

    // "Computation" for the timestep
    if (sleep) {
#ifdef PROFILE
      startComp();
#endif

      do_sleep(sleep);

#ifdef PROFILE
      endComp();
#endif
    }

#ifdef PROFILE
    startCommExchange();
#endif

    // perform communication
    int partner;
    if (rank < numprocs / 2) {
      partner = partners[rank];
      MPI_Send(sendBuf, msgSize, MPI_BYTE, partner, 0, comm);
    }
    else {
      MPI_Recv(recvBuf, msgSize, MPI_BYTE, MPI_ANY_SOURCE, 0, comm, MPI_STATUS_IGNORE);
    }

#ifdef PROFILE
    endCommExchange();
#endif
    SCOREP_USER_REGION_BY_NAME_END(region_name);
    if (toTime == i) SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_ForTiming");
    // Synchronize every "iterations" timesteps.
    if ((i + 1) % iterations == 0) {
#ifdef PROFILE
      enterSync();
#endif
      //MPI_Barrier(comm);
#ifdef PROFILE
      exitSync(rank);
#endif
    }
  }
  SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_MainLoop");

  // End timing, clean up, and report time
  double endt = MPI_Wtime();

#ifdef SCOREP_USER_ENABLE
  if (SCOREP_RECORDING_IS_ON()) {
    SCOREP_RECORDING_OFF();
  }
#endif


  double myTime = endt - startt;
  double maxTime;
  MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, comm);

  printf("Rank execution time: %f\n", myTime);
  if (rank == 0) {
    printf("Overall execution time: %f\n", maxTime);
  }

#ifdef PROFILE
  writeProfile(rank, numprocs);
#endif

  free(sendBuf);
  free(recvBuf);
}


int pairs_main(int argc, char**argv, MPI_Comm comm){

  appWorld = comm;

  int num_ranks, rank;

  //Start MPI, get num_ranks
  MPI_Comm_size(appWorld, &num_ranks);
  if(num_ranks == 0){
    printf("MPI_Comm_size failure\n");
    exit(1);
  }

  //Get global rank
  MPI_Comm_rank(appWorld, &rank);

  if (argc != 7) {
    if (rank == 0) printf("Usage: %s <message size> <iterations> <phases> <comp time per iter> <comp imbalance> <random seed>\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  int msgSize, iterations, numPhases, maxSleepTime, sleepTime = 0, compImbalance, seed;
  msgSize = atoi(argv[1]);
  iterations = atoi(argv[2]);
  numPhases = atoi(argv[3]);
  maxSleepTime = atoi(argv[4]);
  compImbalance = atoi(argv[5]);
  seed = atoi(argv[6]);

  srand(rank + seed);
  char imbalanceDesc[10];
  switch(compImbalance) {
    case 1: // Small
      sleepTime = rand_range(maxSleepTime * 0.9, maxSleepTime);
      strcpy(imbalanceDesc, "Small");
      break;
    case 2: // Medium
      sleepTime = rand_range(0, maxSleepTime);
      strcpy(imbalanceDesc, "Medium");
      break;
    case 3: // Large
      if (rank == 0) sleepTime = maxSleepTime;
      else sleepTime = 0;
      strcpy(imbalanceDesc, "Large");
      break;
    default:
      if (rank == 0) printf("Invalid computation imbalance. Valid values are 1 (small), 2 (medium), and 3 large)\n");
      MPI_Abort(MPI_COMM_WORLD, 2);
      break;
  }

#ifdef NN_DEBUG
  if (rank == 0) {
    printf("Message size: %d bytes\n", msgSize);
    printf("Iterations: %d\n", iterations);
    printf("Phases: %d\n", numPhases);
    printf("Max computation time: %d ns\n", maxSleepTime);
    printf("Imbalance: %s\n", imbalanceDesc);
    printf("Random seed: %d\n", seed);
  }
  printf("Rank %d computation time: %d ns\n", rank, sleepTime);
  fflush(stdout);
#endif

  p2p(appWorld, num_ranks, rank, msgSize, numPhases, iterations, sleepTime);

  print_hosts();
  return 0;
}

