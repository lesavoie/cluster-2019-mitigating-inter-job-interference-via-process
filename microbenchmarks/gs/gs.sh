#!/bin/bash -x

#MSUB -q pbatch
#MSUB -l nodes=20
#MSUB -l partition=catalyst
#MSUB -l walltime=00:05:00
#MSUB -V
#MSUB -m ae

NODES=20
RANKS=$[$NODES * 1]
MSG_SIZE=$[64 * 1024]
ITERS=27
SLEEP=2000000
IMBALANCE=2 # 1=small, 2=med, 3=large
SEED=2

export SCOREP_ENABLE_TRACING=true

date
srun -l -N $NODES -n $RANKS ~/benchmarks/gs/globalsync $MSG_SIZE $ITERS $SLEEP $IMBALANCE $SEED
date

