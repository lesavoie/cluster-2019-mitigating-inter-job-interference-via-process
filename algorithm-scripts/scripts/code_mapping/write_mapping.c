#include <stdio.h>
#include <stdlib.h>

//#define DEBUG

// Usage: ./write_mapping <numjobs> global.txt <global0.txt> <global1.txt> ... <job0.txt> <job1.txt> ...

int main(int argc, char**argv) {
  // Write global and job map files. Print contents for debugging if DEBUG flag is on.
  char line[20];
  int len = 20;

  int numjobs = atoi(argv[1]);

  // Global for both jobs.
  FILE *binout = fopen("global.bin", "wb");
  FILE *txtin = fopen(argv[2], "r");

  while (fgets(line, len, txtin) != NULL) {
    int globalrank, localrank, jobid;
    sscanf(line, "%d %d %d", &globalrank, &localrank, &jobid);
    // Global map file: globalrank, localrank, jobid
    fwrite(&globalrank, sizeof(int), 1, binout);
    fwrite(&localrank, sizeof(int), 1, binout);
    fwrite(&jobid, sizeof(int), 1, binout);

#ifdef DEBUG
    printf("%d %d %d\n", globalrank, localrank, jobid);
#endif
  }
  // Clean up: close files.
  fclose(binout);
  fclose(txtin);


  // Files for each job (global and job file).
  int i;
  for (i = 0; i < numjobs; i++) {
    char binoutname[20];
    char jobname[20];
    sprintf(binoutname, "global%d.bin", i);
    sprintf(jobname, "job%d", i);

    FILE *binout = fopen(binoutname, "wb");
    FILE *job = fopen(jobname, "wb");

    FILE *txtin = fopen(argv[3 + i], "r");
    FILE *jobin = fopen(argv[3 + numjobs + i], "r");

    // Global for job 0.
    while (fgets(line, len, txtin) != NULL) {
      int globalrank, localrank, jobid;
      sscanf(line, "%d %d %d", &globalrank, &localrank, &jobid);
      // Global map file: globalrank, localrank, jobid
      fwrite(&globalrank, sizeof(int), 1, binout);
      fwrite(&localrank, sizeof(int), 1, binout);
      fwrite(&jobid, sizeof(int), 1, binout);
    }

    // Job 0.
    while (fgets(line, len, jobin) != NULL) {
      int globalrank;
      sscanf(line, "%d", &globalrank);
      // Write to the job map file for job 0. 
      fwrite(&globalrank, sizeof(int), 1, job);
    }

    // Clean up: close files.
    fclose(binout);
    fclose(txtin);
    fclose(job);
    fclose(jobin);
  }
}
