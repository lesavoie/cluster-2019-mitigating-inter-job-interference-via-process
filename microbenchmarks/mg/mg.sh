#!/bin/bash

#MSUB -q pbatch
#MSUB -l nodes=32
#MSUB -l partition=catalyst
#MSUB -l walltime=00:20:00
#MSUB -V
#MSUB -m ae

NODES=32
RANKS=$[$NODES * 1]
MSG_SIZE=$[64 * 1024 * 2]
ITERS=5
SLEEP=10000000
IMBALANCE=2 # 1=small, 2=med, 3=large
SEED=2

export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=163840000

date
srun -l -N $NODES -n $RANKS ~/benchmarks/mg/miniAMG $MSG_SIZE $ITERS $SLEEP $IMBALANCE $SEED
date
