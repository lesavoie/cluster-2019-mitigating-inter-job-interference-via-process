#!/bin/bash -x

#MSUB -q pbatch
#MSUB -l nodes=20
#MSUB -l partition=catalyst
#MSUB -l walltime=00:20:00
#MSUB -V
#MSUB -m ae

NODES=20
RANKS=$[$NODES * 1]
MSG_SIZE=$[64 * 1024 * 16]
ITERATIONS=120
PHASES=1
COMP_TIME=400000
IMBALANCE=2 # 1=small, 2=med, 3=large
SEED=2

export SCOREP_ENABLE_TRACING=true

date
srun -l -N $NODES -n $RANKS ~/benchmarks/rp/rp $MSG_SIZE $ITERATIONS $PHASES $COMP_TIME $IMBALANCE $SEED
date
