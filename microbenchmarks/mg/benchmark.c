#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <scorep/SCOREP_User.h>
#include "mpi.h"

int MessageTable[10][32][32];

#define TAG 13

void exchange(int, int, int);
void createMessageTable();

int rand_range(int M, int N) {
  return M + rand() / (RAND_MAX / (N - M + 1) + 1);
}

int main(int argc, char ** argv) {

  int i, phase, iterations, rank, ranks, maxSleepTime, sleepTime = 0, compImbalance, seed, messageBytes;
  struct timespec compTime;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &ranks);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (argc != 6) {
    if (rank == 0) printf("Usage: %s <message size> <iterations> <comp time per iter> <comp imbalance> <random seed>\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  messageBytes = atoi(argv[1]) / 4;
  iterations = atoi(argv[2]);
  maxSleepTime = atoi(argv[3]);
  compImbalance = atoi(argv[4]);
  seed = atoi(argv[5]);

  srand(rank + seed);
  char imbalanceDesc[10];
  switch(compImbalance) {
    case 1: // Small
      sleepTime = rand_range(maxSleepTime * 0.9, maxSleepTime);
      strcpy(imbalanceDesc, "Small");
      break;
    case 2: // Medium
      sleepTime = rand_range(0, maxSleepTime);
      strcpy(imbalanceDesc, "Medium");
      break;
    case 3: // Large
      if (rank == 0) sleepTime = maxSleepTime;
      else sleepTime = 0;
      strcpy(imbalanceDesc, "Large");
      break;
    default:
      if (rank == 0) printf("Invalid computation imbalance. Valid values are 1 (small), 2 (medium), and 3 large)\n");
      MPI_Abort(MPI_COMM_WORLD, 3);
      break;
  }

  if (rank == 0) {
    printf("Message size: %d bytes\n", messageBytes);
    printf("Iterations: %d\n", iterations);
    printf("Max computation time: %d ns\n", maxSleepTime);
    printf("Imbalance: %s\n", imbalanceDesc);
    printf("Random seed: %d\n", seed);
  }
  printf("Rank %d computation time: %d ns\n", rank, sleepTime);
  fflush(stdout);

  // There are 10 phases per iteration, so divide the computation time evenly between them
  sleepTime /= 10;

  compTime.tv_sec = 0;
  compTime.tv_nsec = sleepTime;

  createMessageTable();

  SCOREP_USER_REGION_BY_NAME_BEGIN("TRACER_WallTime_MainLoop", SCOREP_USER_REGION_TYPE_COMMON);
  for (i = 0; i < iterations; i++) {
    char region_name[30];
    sprintf(region_name, "TRACER_WallTime_Iteration%d", i);
    double startTime = MPI_Wtime();
    SCOREP_USER_REGION_BY_NAME_BEGIN(region_name, SCOREP_USER_REGION_TYPE_COMMON);
    for (phase = 0; phase < 10; phase++) {
      if (sleepTime > 0) nanosleep(&compTime, NULL);
      exchange(rank, phase, messageBytes);
    }
    SCOREP_USER_REGION_BY_NAME_END(region_name);
    double endTime = MPI_Wtime();
    printf("Rank %d iteration %d time: %f sec\n", rank, i, endTime - startTime);
  }
  SCOREP_USER_REGION_BY_NAME_END("TRACER_WallTime_MainLoop");

  MPI_Barrier(MPI_COMM_WORLD);
  printf("i am %d, done\n", rank);

  MPI_Finalize();
  return 0;
}

void exchange(int rank, int phase, int count) {
  int messageDataSend[32][count];
  int messageDataRecv[32][count];
  MPI_Request mpiReq[64];
  int j, numRequests, indexR = 0, indexS = 0;

  for (j = 0; j < 32; j++) {
    if (MessageTable[phase][j][rank]) { 
      MPI_Irecv(messageDataRecv[indexR], count, MPI_INT, j, TAG, MPI_COMM_WORLD, &mpiReq[indexR]);
      indexR++;
    }
  }

  for (j = 0; j < 32; j++) {
    if (MessageTable[phase][rank][j]) { 
      MPI_Isend(messageDataSend[indexS], count, MPI_INT, j, TAG, MPI_COMM_WORLD, &mpiReq[indexR+indexS]);
      indexS++;
    }
  }

  numRequests = indexR + indexS;
  if (numRequests > 0) {
    MPI_Waitall(numRequests, mpiReq, MPI_STATUSES_IGNORE);
  }
}
 
