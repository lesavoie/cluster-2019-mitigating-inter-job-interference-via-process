*** Usage:

>> make
>> python tracer-mapping.py <slurm hostfile> <num jobs> <ranks per node>


*** Outputs:

global.bin, global0.bin, global1.bin, ..., job0, job1, ...
  (One global mapping file, one global file per job, and one job file per job.
   These are the mapping files that TraceR requires.)


*** Example test:

>> python tracer-mapping.py hosts.txt 2 22

Here hosts.txt was taken from catalyst with two programs run on 22 ranks per
node, 75 nodes each.


*** Host file format:

  <host for job 0, rank 0>
  <host for job 0, rank 1>
  <host for job 0, rank 2>
  .
  .
  .
  <host for job 0, rank n>
  <host for job 1, rank 0>
  <host for job 1, rank 1>
  <host for job 1, rank 2>
  .
  .
  .
  <host for job 1, rank n>
  etc for more jobs...


For example, if job 0 ran on catalyst1 and catalyst3 and job 1 ran on
catalyst2 and catalyst4, each with 4 ranks per node, the hostfile would be:
  catalyst1
  catalyst1
  catalyst1
  catalyst1
  catalyst3
  catalyst3
  catalyst3
  catalyst3
  catalyst2
  catalyst2
  catalyst2
  catalyst2
  catalyst4
  catalyst4
  catalyst4
  catalyst4



OTHER NOTES:
* tracer-mapping.py assumes all jobs had the same number of nodes.

* It assumes the nodes are laid out in linear order of node id on the
switches (i.e. catalyst1 is the first node on the first switch, catalyst2
is the second node on the first switch, catalyst18 is the first node on
the second switch, etc...

* The script writes the TraceR mapping files out in human-readable format
(global.txt, global*.txt, job*.txt), then calls write_mapping to convert
them to the necessary binary format.

* If the job did not use all available cores per node, the script
distributes them evenly across the sockets, leaving the highest cores
on each socket vacant. In the example where jobs were run on catalyst
with 22 ranks per node out of the available 24, the mapping is

            Socket 0                              Socket 1
0  1  2  3  4  5  6  7  8  9  10  X    11 12 13 14 15 16 17 18 19 20 21  X

where numbers are rank ids and X's are vacant core.
